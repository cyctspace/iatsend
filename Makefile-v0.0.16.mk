#========================================
# A Simple Mafile for C/C++ Program
# Author: numax
# Date: 2015/06/28
#========================================


#========================================
# Get Current Operating System Name
#========================================
OS_NAME := $(shell uname -s)

RM := rm -rf
ECHO := echo
COPY := cp -f
MKDIR := mkdir
TOUCH := touch

XCC_PREFIX := aarch64-unisoc-linux-
XCC_TOOLCHAINS := /opt/toolchains
XCC_BINPATH := $(XCC_TOOLCHAINS)/aarch64-unisoc-linux/recipe-sysroot-native/usr/bin/aarch64-unisoc-linux/
XCC_SYSROOT := $(XCC_TOOLCHAINS)/aarch64-unisoc-linux/recipe-sysroot

export PATH := $(XCC_BINPATH):${PATH}

#========================================
# C Compiler and Options 
#========================================
CC := $(XCC_PREFIX)gcc
CDEF1 := -I 

CDEFS := -I ./
CDEFS += -I ./inc
CDEFS += -I $(XCC_SYSROOT)/usr/include
CFLAGS := -O0 -g $(CDEFS)

#========================================
# C Pragram Pre-treatment and Options 
#========================================
CPP := $(XCC_PREFIX)cpp
CPPFLAGS :=

#========================================
# C++ Compiler and Compile-Options 
#========================================
CXX := $(XCC_PREFIX)g++
CXXDEF1 :=
CXXDEF2 :=
CXXDEFS := $(CXXDEF1) $(CXXDEF2)
CXXFLAGS := -O0 -g $(CXXDEFS)

#========================================
# C++ Pragram Pre-treatment and Options 
#========================================
CXXPP := $(XCC_PREFIX)cpp
CXXPPFLAGS :=

#========================================
# Link flags and essential third librarys
#========================================
LD := $(XCC_PREFIX)ld

LDFLAGS := -e main

LDFLAGS += -lc

LDFLAGS += -L ./
LDFLAGS += -L ./lib/

LDFLAGS += --sysroot="$(XCC_SYSROOT)"

LDLIBS := -lc

#========================================
# All information related to time 
#========================================
XDBG_NOW := $(shell date "+%Y %m %d %H %M %S %z")

#========================================
# Current Project Name
#========================================
XDBG_CPN := iatsend

#========================================
# All directory for compiling 
#========================================
XDBG_RTDIR := xdbg
XDBG_MYDIR := $(XDBG_RTDIR)/$(XDBG_CPN)
XDBG_DIRS := $(XDBG_RTDIR) $(XDBG_MYDIR)

XDBG_HFS := cyct_btime.h
XDBG_HFS := $(addprefix $(XDBG_MYDIR)/, $(XDBG_HFS))

XDBG_SRC = iatsend.c \
           cyct_xstring.c \
           cyct_dstring.c \
           cyct_iotty.c \
           cyct_time.c \
           cyct_debug.c \
           cyct_runlog.c \
           cyct_version.c \
           cyct_getopt.c \
           cyct_atcmd.c

XDBG_OBJS := $(addsuffix .o, $(basename $(XDBG_SRC)))
XDBG_OBJS := $(addprefix $(XDBG_MYDIR)/, $(XDBG_OBJS))
XDBG_DEPS := $(addsuffix .dep, $(XDBG_OBJS))

XDBG_BIN := $(addprefix $(XDBG_MYDIR)/, $(XDBG_CPN))

all:$(XDBG_DIRS) new $(XDBG_BIN)
	@$(ECHO) ================OK================

$(XDBG_DIRS):
	$(MKDIR) $@

$(XDBG_BIN):$(XDBG_OBJS)
	$(LD) -o $@ $(XDBG_OBJS) $(LDLIBS) $(LDFLAGS)

$(XDBG_MYDIR)/%.o:%.c
	$(CC) -c $< -o $@ -MD -MF $@.dep -MP $(CFLAGS)


.PHONY:new check clean cleanall install
new:
	-@$(RM) $(XDBG_HFS)
	@$(ECHO) '#ifndef __CYCT_BTIME_H__' >> $(XDBG_HFS)
	@$(ECHO) '#define __CYCT_BTIME_H__' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_NOW ' >> $(XDBG_HFS)
	@$(ECHO) -n '"' >> $(XDBG_HFS)
	@$(ECHO) -n $(XDBG_NOW) >> $(XDBG_HFS)
	@$(ECHO) '"' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)
	@$(ECHO) '#endif /* __CYCT_BTIME_H__ */' >> $(XDBG_HFS)
	@$(COPY) $(XDBG_HFS) $(CURDIR)

check: 
	@$(ECHO) OS_NAME: $(OS_NAME)
	@$(ECHO) CC: $(CC)
	@$(ECHO) CFLAGS: $(CFLAGS)
	@$(ECHO) CPP: $(CPP)
	@$(ECHO) CPPFLAGS: $(CPPFLAGS)
	@$(ECHO) CXX: $(CXX)
	@$(ECHO) CXXFLAGS: $(CXXFLAGS)
	@$(ECHO) CXXPP: $(CXXPP)
	@$(ECHO) CXXPPFLAGS: $(CXXPPFLAGS)
	@$(ECHO) $(shell uname -a)

clean:
	-$(RM) $(XDBG_MYDIR)
	
cleanall:
	-$(RM) $(XDBG_RTDIR)

install:
	@$(ECHO) !!!Sorry, Unsupported Now!!!


-include $(XDBG_DEPS)
