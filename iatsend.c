#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_time.h"
#include "cyct_debug.h"
#include "cyct_version.h"
#include "cyct_xstring.h"
#include "cyct_dstring.h"
#include "cyct_iotty.h"
#include "cyct_atcmd.h"
#include "cyct_getopt.h"
#include "iatsend.h"

static int gi_id_iotty = 0;
static int gi_time_maxwait = 3000;
static char gp_cur_atcmd[8192]={0};

int cyct_iatsend_f_usage()
{
    char* p_xsw_btime = NULL;
    char* p_xsw_version = NULL;

    p_xsw_btime = cyct_btime_f_get();
    p_xsw_version = cyct_version_f_get();
    printf("------------------------------------------------------\r\n");
    printf("Name: iatsend-%s\r\n", p_xsw_version);
    printf("Build: %s\r\n", p_xsw_btime);
    printf("Author: tangtao\r\n");
    printf("***Send \"AT+CPIN?\" to /dev/ttyUSB1***\r\n");
    printf("Usage: iatsend 1 \"AT+CPIN?\"\r\n");
    printf("***Send \"AT+CPIN?\" to COM1***\r\n");
    printf("Usage: iatsend.exe 1 \"AT+CPIN?\"\r\n");
    printf("------------------------------------------------------\r\n");

    OS_DBG_LOG((NULL, "------------------------------------------------------\r\n"));
    OS_DBG_LOG((NULL, "Name: iatsend-%s\r\n", p_xsw_version));
    OS_DBG_LOG((NULL, "Build: %s\r\n", p_xsw_btime));
    OS_DBG_LOG((NULL, "Author: tangtao\r\n"));
    OS_DBG_LOG((NULL, "***Send \"AT+CPIN?\" to /dev/ttyUSB1***\r\n"));
    OS_DBG_LOG((NULL, "Usage: iatsend 1 \"AT+CPIN?\"\r\n"));
    OS_DBG_LOG((NULL, "***Send \"AT+CPIN?\" to COM1***\r\n"));
    OS_DBG_LOG((NULL, "Usage: iatsend.exe 1 \"AT+CPIN?\"\r\n"));
    OS_DBG_LOG((NULL, "------------------------------------------------------\r\n"));
    return 0;
}

int cyct_iatsend_f_main(s32_t i_id_device, char* p_cur_atreq, s32_t i_time_maxwait)
{
    void* p_tty_ctxt = NULL;

    s32_t i_err_code = 0;
    s32_t i_nb_curread = 0;
    s32_t i_nb_curwrite = 0;
    cyct_dstring_t* p_buf_allread = NULL;
    cyct_dstring_t* p_buf_allwait = NULL;

    char p_buf_data[128];

    OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,entry\r\n"));
    OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,i_id_device=%d\r\n", i_id_device));
	OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,p_cur_atreq=%s\r\n", p_cur_atreq));
	OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,i_time_maxwait=%d\r\n", i_time_maxwait));
    p_tty_ctxt = cyct_iotty_open(i_id_device);
    if (NULL == p_tty_ctxt)
    {
        OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,p_tty_ctxt=NULL\r\n"));
        OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,exit\r\n"));
        return -1;
    }

    #ifdef CYCT_WINDOWS
        cyct_time_f_sleep(100);
    #endif /* CYCT_WINDOWS */

    p_buf_allread = cyct_dstring_f_new(p_buf_data, i_nb_curread);
    i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_data, sizeof(p_buf_data));
    while (i_nb_curread > 0)
    {
        cyct_dstring_f_append(p_buf_allread, p_buf_data, i_nb_curread);
        i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_data, sizeof(p_buf_data));
    }
    cyct_prompt_f_dprintf(p_buf_allread, 0);
    OS_DBG_LOG(("CYCT:IATSEND:ATCMD:RCV:", "%s\r\n", p_buf_allread));
    cyct_dstring_f_delete(p_buf_allread);

    OS_DBG_LOG(("CYCT:IATSEND:ATCMD:SND:", "%s\r\n", p_cur_atreq));
	sprintf(p_buf_data, "%s\r", p_cur_atreq);
    i_nb_curwrite = cyct_iotty_write(p_tty_ctxt, p_buf_data, strlen(p_buf_data));
    cyct_prompt_f_xprintf(p_buf_data, 0);

    p_buf_allwait = cyct_atcmd_f_wait1(p_tty_ctxt, "\r\nOK\r\n", i_time_maxwait, &i_err_code);
    cyct_prompt_f_dprintf(p_buf_allwait, 0);
    OS_DBG_LOG(("CYCT:IATSEND:ATCMD:RCV:", "%s\r\n", p_buf_allwait->pt_string));
    cyct_dstring_f_delete(p_buf_allwait);

    cyct_iotty_close(p_tty_ctxt);
    OS_DBG_LOG((NULL, "CYCT:IATSEND:cyct_iatsend_f_main,exit\r\n"));

	return 0;
}

static int cyct_iatsend_f_getopt(int argc, char **argv)
{
	int i_op_status = 0;

	int c;
	int digit_optind = 0;

	while (1)
    {
		int this_option_optind = optind ? optind : 1;

		c = cyct_argx_f_getopt(argc, argv, "c:d:t:");
		if (c == EOF)
			break;

		switch (c)
		{
		case 'c':
			strcpy(gp_cur_atcmd, optarg);
			break;

		case 'd':
			gi_id_iotty = atoi(optarg);
			break;

        case 'h':
            i_op_status = -1;
            break;

		case 't':
			gi_time_maxwait = atoi(optarg);
			break;

		case '?':
			i_op_status = -1;
			break;

		default:
			i_op_status = -1;
			break;
		}
    }

	return i_op_status;
}

int main(int argc, char * argv[])
{
	s32_t i_op_status = 0;

    s32_t i_id_iotty = 0;
    s32_t i_time_maxwait = 3000;

    char* p_cur_atreq = NULL;
    char* p_xsw_version = NULL;

    cyct_runlog_f_init(NULL);
    cyct_time_f_tickinit();

    i_op_status = cyct_iatsend_f_getopt(argc, argv);
	if (i_op_status == -1)
	{
		cyct_iatsend_f_usage();
        cyct_runlog_f_reset();
        return 0;
    }

	i_id_iotty = gi_id_iotty;
	i_time_maxwait = gi_time_maxwait;
	p_cur_atreq = gp_cur_atcmd;

    p_xsw_version = cyct_version_f_get();
    OS_DBG_LOG((NULL, "CYCT:info:iatsend-%s\r\n", p_xsw_version));

    OS_DBG_LOG((NULL, "--------------------------------\r\n"));
    OS_DBG_LOG((NULL, "CYCT:main:entry\r\n"));
    cyct_iatsend_f_main(i_id_iotty, p_cur_atreq, i_time_maxwait);
    OS_DBG_LOG((NULL, "CYCT:main:exit\r\n"));
    OS_DBG_LOG((NULL, "--------------------------------\r\n"));
    cyct_runlog_f_reset();

    return 0;
}
