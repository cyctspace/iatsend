#ifndef __CYCT_TIME_H__
#define __CYCT_TIME_H__

u32_t cyct_time_f_tickinit();
u32_t cyct_time_f_tickget();
u32_t cyct_time_f_sleep(u32_t i_cnt_msecs);
u32_t cyct_time_f_usleep(u32_t i_cnt_usecs);

#endif /* __CYCT_TIME_H__ */

