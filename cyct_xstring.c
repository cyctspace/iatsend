#include "cyct_config.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"
#include "cyct_xstring.h"


/*
 *  Description:
 *    Get expected string from a specified string.
 *  Param: s, an ASCII string;
 *  Param: o, offsets
 *  Param: c, delimiter
 *  Param: buf, a buffer
 *  Return: p_ret_addr, NULL, fail;NON-NULL, success.
 *  History:
 */
char * cyct_xstring_f_strtok(const char * s, s32_t o, char c, char * buf)
{
    char * p_ret_addr = NULL;

    s32_t i = 0;
    s32_t i_str_len =0;

    if ((NULL == s) || (NULL == buf) || ('\0' == c))
    {
        return p_ret_addr;
    }

    i_str_len = strlen(s);
    i = o;
    while (i < i_str_len)
    {
        if (*(s+i) == c)
        {
            p_ret_addr = (char *)(s + i +1);
            break;
        }

        buf[i] = s[i];
        i++;
    }
    buf[i] = '\0';

    return p_ret_addr;
}

/*
 *  Description:
 *    UpCase all characters that in the specified string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_op_status, 0, fail;1, success.
 *  History:
 */
s32_t cyct_xstring_f_strkupcase(u08_t * s, u08_t k)
{
    s32_t i_op_status = 0;

    s32_t i = 0;
    s32_t i_str_len =0;

    if ((NULL == s))
    {
        return i_op_status;
    }

    while ('\0' != s[i])
    {
        if (k == s[i])
        {
            break;
        }

        if ((s[i] >= 'a') && (s[i] <= 'z'))
        {
            s[i] = s[i] - 'a' + 'A';
        }

        i++;
    }

    i_op_status = 1;
    return i_op_status;
}

/*
 *  Description:
 *    Get the first position of a specified character.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_pos_keychar, -1, fail; >= 0, success.
 *  History:
 */
s32_t cyct_xstring_f_strkpos(const u08_t * s, char k)
{
    s32_t i_pos_keychar = -1;
    s32_t i = 0;
 
    if (NULL == s)
    {
        return i_pos_keychar;
    }
    
    while ('\0' != s[i])
    {
        if (k == s[i])
        {
            i_pos_keychar = i;
            break;
        }
        
        i++;
    }

    return i_pos_keychar;
}

/*
 *  Description:
 *    digital string or not.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: is_digital, 0, false; 1, true.
 *  History:
 */
s32_t cyct_xstring_f_isdigital(const u08_t * s, s32_t l)
{
    s32_t i_is_digital = 0;
    s32_t i = 0;
 
    if (NULL == s)
    {
        return i_is_digital;
    }
    
    i_is_digital = 1;
    while (i < l)
    {
        if ((s[i] < '0') || (s[i] > '9'))
        {
            break;
        }

        i++;
    }

    while (i < l)
    {
        if (s[i] != ' ')
        {
            i_is_digital = 0;
            break;
        }

        i++;
    }

    return i_is_digital;
}

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_strlwr(const char * s, s32_t l)
{
    s32_t i = 0;

    s32_t i_len_str = 0;
    char * p_cur_str = NULL;

    p_cur_str = (char *)s;
    if (NULL == p_cur_str)
    {
        return NULL;
    }

    i_len_str = l;
    if (0 == i_len_str)
    {
        i_len_str = strlen(p_cur_str);
    }

    while (i < i_len_str)
    {
        p_cur_str[i] = tolower(p_cur_str[i]);
        i++;
    }

    return (char *)s;
}

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_strupr(const char * s, s32_t l)
{
    s32_t i = 0;

    s32_t i_len_str = 0;
    char * p_cur_str = NULL;

    p_cur_str = (char *)s;
    if (NULL == p_cur_str)
    {
        return NULL;
    }

    i_len_str = l;
    if (0 == i_len_str)
    {
        i_len_str = strlen(p_cur_str);
    }

    while (i < i_len_str)
    {
        p_cur_str[i] = toupper(p_cur_str[i]);
        i++;
    }

    return (char *)s;

}


/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_delchar(const char * s, char k)
{
    s32_t i = 0;
    s32_t j = 0;

    s32_t i_len_str = 0;
    char * p_cur_str = NULL;

    p_cur_str = (char *)s;
    if (NULL == p_cur_str)
    {
        return NULL;
    }

    i_len_str = strlen(p_cur_str);

    while (i < i_len_str)
    {
        if (p_cur_str[i] != k)
        {
            p_cur_str[j] = p_cur_str[i];
            j++;
        }

        i++;
    }

	p_cur_str[j] = 0;
    return (char *)s;
}

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_lstrip(const char * s)
{
    s32_t i = 0;
    s32_t j = 0;

    s32_t i_len_str = 0;
    char* p_cur_str = NULL;

    p_cur_str = (char *)s;
    if (NULL == p_cur_str)
    {
        return NULL;
    }

    i_len_str = strlen(p_cur_str);
    while (i < i_len_str)
    {
        if (   (p_cur_str[i] != ' ')
           && (p_cur_str[i] != '\r')
           && (p_cur_str[i] != '\n')
           && (p_cur_str[i] != '\t'))
        {
            break;
        }

        i++;
    }

    i_len_str = i_len_str - i;
    memmove(p_cur_str, &(p_cur_str[i]), i_len_str);
    p_cur_str[i_len_str] = 0;
    return (char *)s;
}

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_rstrip(const char * s)
{
    s32_t i = 0;
    s32_t j = 0;

    s32_t i_len_str = 0;
    char * p_cur_str = NULL;

    p_cur_str = (char *)s;
    if (NULL == p_cur_str)
    {
        return NULL;
    }

    i_len_str = strlen(p_cur_str);
    i = i_len_str - 1;
    while (i > 0)
    {
        if (   (p_cur_str[i] != ' ')
           && (p_cur_str[i] != '\r')
           && (p_cur_str[i] != '\n')
           && (p_cur_str[i] != '\t'))
        {
            p_cur_str[i+1] = 0;
            break;
        }

        i--;
    }

    return (char *)s;
}

/*
 *  Description:
 *    Converts the hex to bin.
 *  Param: hex_ptr, the hexadecimal format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: bin_ptr, pointer to the binary format string;
 *  Return: FALSE, there's invalid character;
 *  Note: NONE
 */
s32_t cyct_xstring_f_hex2bin(const u08_t *hex_ptr, s32_t length, u08_t *bin_ptr)
{
    u08_t        *dest_ptr = bin_ptr;
    s32_t        i = 0;
    u08_t        ch;

    for(i = 0; i < length; i += 2)
    {
        // the bit 8,7,6,5
        ch = hex_ptr[i];

        // digital 0 - 9
        if(ch >= '0' && ch <= '9')
        {
            *dest_ptr = (u08_t)((ch - '0') << 4);
        }
        // a - f
        else if(ch >= 'a' && ch <= 'f')
        {
            *dest_ptr = (u08_t)((ch - 'a' + 10) << 4);
        }
        // A - F
        else if(ch >= 'A' && ch <= 'F')
        {
            *dest_ptr = (u08_t)((ch - 'A' + 10) << 4);
        }
        else
        {
            return 0;
        }

        // the bit 1,2,3,4
        ch = hex_ptr[i+1];

        // digtial 0 - 9
        if(ch >= '0' && ch <= '9')
        {
            *dest_ptr |= (u08_t)(ch - '0');
        }
        // a - f
        else if(ch >= 'a' && ch <= 'f')
        {
            *dest_ptr |= (u08_t)(ch - 'a' + 10);
        }
        // A - F
        else if(ch >= 'A' && ch <= 'F')
        {
            *dest_ptr |= (u08_t)(ch - 'A' + 10);
        }
        else
        {
            return 0;
        }

        dest_ptr++;
    }

    return 1;

}

/*
 *  Description:
 *    Converts the bin to hex.
 *  Param: bin_ptr, the binary format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: hex_ptr, pointer to the hexadecimal format string;
 *  Return: 1, success;
 *  Note: NONE
 */
s32_t cyct_xstring_f_bin2hex(const u08_t *bin_ptr, s32_t length, u08_t *hex_ptr)
{
    u08_t        semi_octet;
    s32_t        i;

    for(i = 0; i < length; i++)
    {
        // get the high 4 bits
        semi_octet = (u08_t)((bin_ptr[i] & 0xF0) >> 4);

        if(semi_octet <= 9)  //semi_octet >= 0
        {
            *hex_ptr = (u08_t)(semi_octet + '0');
        }
        else
        {
            *hex_ptr = (u08_t)(semi_octet + 'A' - 10);
        }

        hex_ptr++;

        // get the low 4 bits
        semi_octet = (u08_t)(bin_ptr[i] & 0x0f);

        if(semi_octet <= 9)  // semi_octet >= 0
        {
            *hex_ptr = (u08_t)(semi_octet + '0');
        }
        else
        {
            *hex_ptr = (u08_t)(semi_octet + 'A' - 10);
        }

        hex_ptr++;
    }

    return 1;
}

