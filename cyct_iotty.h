#ifndef __CYCT_IOTTY_H__
#define __CYCT_IOTTY_H__

void* cyct_iotty_open(s32_t i_id_device);
s32_t cyct_iotty_write(void* p_fd_iotty, u08_t* p_buf_data, s32_t i_len_data);
s32_t cyct_iotty_read(void* p_fd_iotty, u08_t* p_buf_data, s32_t i_len_tryread);
s32_t cyct_iotty_close(void* p_fd_iotty);
s32_t cyct_iotty_rxclean(void* p_fd_iotty);

#endif /* __CYCT_IOTTY_H__ */

