#ifndef __CYCT_XSTRING_H__
#define __CYCT_XSTRING_H__

/*
 *  Description:
 *    Get expected string from a specified string.
 *  Param: s, an ASCII string;
 *  Param: o, offsets
 *  Param: c, delimiter
 *  Param: buf, a buffer
 *  Return: p_ret_addr, NULL, fail;NON-NULL, success.
 *  History:
 */
char * cyct_xstring_f_strtok(const char * s, s32_t o, char c, char * buf);

/*
 *  Description:
 *    Get the first position of a specified character.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_pos_keychar, -1, fail; >= 0, success.
 *  History:
 */
s32_t cyct_xstring_f_strkpos(const u08_t * s, char k);

/*
 *  Description:
 *    UpCase all characters that in the specified string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_op_status, 0, fail;1, success.
 *  History:
 */
s32_t cyct_xstring_f_strkupcase(u08_t * s, u08_t k);

/*
 *  Description:
 *    digital string or not.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: is_digital, 0, false; 1, true.
 *  History:
 */
s32_t cyct_xstring_f_isdigital(const u08_t * s, s32_t l);

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char * cyct_xstring_f_strlwr(const char * s, s32_t l);

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char * cyct_xstring_f_strupr(const char * s, s32_t l);

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_delchar(const char * s, char k);

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_lstrip(const char * s);

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyct_xstring_f_rstrip(const char * s);

/*
 *  Description:
 *    Converts the hex to bin.
 *  Param: hex_ptr, the hexadecimal format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: bin_ptr, pointer to the binary format string;
 *  Return: FALSE, there's invalid character;
 *  Note: NONE
 */
s32_t cyct_xstring_f_hex2bin(const u08_t * p_hex_string, s32_t i_hex_length, u08_t * p_bin_buf);

/*
 *  Description:
 *    Converts the bin to hex.
 *  Param: bin_ptr, the binary format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: hex_ptr, pointer to the hexadecimal format string;
 *  Return: 1, success;
 *  Note: NONE
 */
s32_t cyct_xstring_f_bin2hex(const u08_t * p_bin_string, s32_t i_bin_length, u08_t * p_hex_buf);

#endif /* __CYCT_XSTRING_H__ */

