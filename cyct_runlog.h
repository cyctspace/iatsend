#ifndef __CYCT_RUNLOG_H__
#define __CYCT_RUNLOG_H__

#define CYCT_DEF_LOG_DIR "runlog"

#define CYCT_LOGFILE_MIN_SIZE ((1024)*(128))
#define CYCT_LOGFILE_MAX_SIZE ((1024)*(512))
#define CYCT_LOGFILE_NEW_CYCLE ((3600)*(24)*(7))

s32_t cyct_runlog_f_init(u08_t* u_rel_dir);
s32_t cyct_runlog_f_fsize(void);
s32_t cyct_runlog_f_printf(const char * vfmt, ...);
s32_t cyct_runlog_f_cprintf(const char * vfmt, ...);
s32_t cyct_runlog_f_myprintf(const char * prefix, const char * vfmt, ...);
s32_t cyct_runlog_f_printfhex(const char * vbuf, s32_t ilen);
s32_t cyct_runlog_f_reset(void);

#endif /* __CYCT_RUNLOG_H__ */

