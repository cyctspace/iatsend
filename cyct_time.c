#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"
#include "cyct_time.h"
#include "cyct_xstring.h"
#include "cyct_dstring.h"
#include "cyct_iotty.h"
#include "cyct_atcmd.h"


#ifdef CYCT_LINUX
#include <unistd.h>
#include <sys/time.h>

static struct timeval g_tv_init;

u32_t cyct_time_f_tickinit()
{
    gettimeofday(&g_tv_init, NULL);
    return 0;
}

u32_t cyct_time_f_tickget()
{
    u32_t i_cnt_curticks = -1;
    struct timeval tv;

    gettimeofday(&tv, NULL);
    i_cnt_curticks = (tv.tv_sec - g_tv_init.tv_sec) * 1000;
    i_cnt_curticks = i_cnt_curticks + (tv.tv_usec - g_tv_init.tv_usec)/1000;

    return i_cnt_curticks;
}

u32_t cyct_time_f_sleep(u32_t i_cnt_msecs)
{
    u32_t i_err_code = -1;
	u32_t i_cnt_usecs = 0;

    i_cnt_usecs = i_cnt_msecs*1000;
    usleep(i_cnt_usecs);

    i_err_code = 0;
    return i_err_code;
}

u32_t cyct_time_f_usleep(u32_t i_cnt_usecs)
{
    u32_t i_err_code = -1;

    usleep(i_cnt_usecs);

    i_err_code = 0;
    return i_err_code;
}
#else /* CYCT_LINUX */
#include <windows.h>

u32_t cyct_time_f_tickinit()
{
    return 0;
}

u32_t cyct_time_f_tickget()
{
    u32_t i_cnt_curticks = -1;

    i_cnt_curticks = clock();

    return i_cnt_curticks;
}

u32_t cyct_time_f_sleep(u32_t i_cnt_msecs)
{
    u32_t i_err_code = -1;
	u32_t i_cnt_usecs = 0;

    Sleep(i_cnt_msecs);

    i_err_code = 0;
    return i_err_code;
}

u32_t cyct_time_f_usleep(u32_t i_cnt_usecs)
{
    u32_t i_err_code = -1;

    i_err_code = 0;
    return i_err_code;
}
#endif /* CYCT_LINUX */
