#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"
#include "cyct_dstring.h"

#define CYCT_MIN_FREE_SIZE 1

/******************************************************************************/
// DESCRIPTION:
//         new a buffer and save the specified data into it.
// PARAM: p_data_buf, a data buffer
// PARAM: p_len_data, the number of bytes data stored in the buffer
// RETURN: p_new_buffer, NULL, fail; NON-NULL, success.
/******************************************************************************/
cyct_dstring_t* cyct_dstring_f_new(u08_t* p_buf_data, s32_t i_len_data)
{
    cyct_dstring_t * p_new_dstr = NULL;
    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if (NULL == p_buf_data)
    {
        return p_new_dstr;
    }

    p_new_dstr = (cyct_dstring_t *)malloc(sizeof(cyct_dstring_t));
    if (NULL == p_new_dstr)
    {
        return p_new_dstr;
    }

    memset(p_new_dstr, 0, sizeof(cyct_dstring_t));
    p_new_dstr->pt_string = (u08_t *)malloc(i_len_data + i_free_size);
    if (NULL == p_new_dstr->pt_string)
    {
        free(p_new_dstr);
        p_new_dstr = NULL;
        return p_new_dstr;
    }

    memset(p_new_dstr->pt_string, 0, i_len_data + i_free_size);
    memcpy(p_new_dstr->pt_string, p_buf_data, i_len_data);
    p_new_dstr->it_length = i_len_data;
    p_new_dstr->it_size = i_len_data + i_free_size;

    return p_new_dstr;
}

/******************************************************************************/
//  DESCRIPTION:
//           save a specified string into the specified buffer.
//  Param: p_lsbuf, specified buffer
//  Param: p_data_buf, a data buffer
//  Param: p_len_data, the number of bytes data stored in the buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_set(cyct_dstring_t* p_a_dstr, u08_t* p_buf_data, s32_t p_len_data)
{
    s32_t i_op_status = 0;

    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if (NULL == p_a_dstr)
    {
        return i_op_status;
    }

    if (NULL == p_a_dstr->pt_string)
    {
        p_a_dstr->pt_string = (u08_t *)malloc(p_len_data + i_free_size);
        if (NULL == p_a_dstr->pt_string)
        {
            return i_op_status;
        }

        p_a_dstr->it_size = p_len_data + i_free_size;
    }
    else
    {
        if (p_len_data > (p_a_dstr->it_size - i_free_size))
        {
            free(p_a_dstr->pt_string);
            p_a_dstr->pt_string = (u08_t *)malloc(p_len_data + i_free_size);
            if (NULL == p_a_dstr->pt_string)
            {
                return i_op_status;
            }

            p_a_dstr->it_size = p_len_data + i_free_size;
        }
    }

    i_op_status = 1;
    memset(p_a_dstr->pt_string, 0, p_len_data + i_free_size);
    memcpy(p_a_dstr->pt_string, p_buf_data, p_len_data);
    p_a_dstr->it_size = p_len_data;

    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           Append and save data into the specified buffer.
//  Param: p_lsbuf, specified buffer
//  Param: p_data_buf, a data buffer
//  Param: p_len_data, the number of bytes data stored in the buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_append(cyct_dstring_t* p_a_dstr, u08_t* p_data_buf, s32_t p_len_data)
{
    s32_t i_op_status = 0;

    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if (NULL == p_a_dstr)
    {
        return i_op_status;
    }

    if (NULL == p_a_dstr->pt_string)
    {
        p_a_dstr->pt_string = (u08_t *)malloc(p_len_data + i_free_size);
        if (NULL == p_a_dstr->pt_string)
        {
            return i_op_status;
        }

        i_op_status = 1;
        p_a_dstr->it_size = p_len_data + i_free_size;
        p_a_dstr->it_length = p_len_data;
        memset(p_a_dstr->pt_string, 0, p_len_data + i_free_size);
        memcpy(p_a_dstr->pt_string, p_data_buf, p_len_data);
    }
    else
    {
        if ((p_len_data + p_a_dstr->it_length)> (p_a_dstr->it_size - i_free_size))
        {
            s32_t u_total_len = 0;
            u08_t * p_tmp_ptr = NULL;

            u_total_len = p_len_data + p_a_dstr->it_length;
            p_tmp_ptr = (u08_t *)malloc(u_total_len + i_free_size);
            if (NULL == p_tmp_ptr)
            {
                return i_op_status;
            }

            i_op_status = 1;
            memset(p_tmp_ptr, 0, u_total_len + i_free_size);
            memcpy(p_tmp_ptr, p_a_dstr->pt_string, p_a_dstr->it_length);
            memcpy(&(p_tmp_ptr[p_a_dstr->it_length]), p_data_buf, p_len_data);

            free(p_a_dstr->pt_string);
            p_a_dstr->pt_string = p_tmp_ptr;
            p_a_dstr->it_length += p_len_data;
            p_a_dstr->it_size = u_total_len + i_free_size;
        }
        else
        {
            i_op_status = 1;
            memcpy(&(p_a_dstr->pt_string[p_a_dstr->it_length]), p_data_buf, p_len_data);
            p_a_dstr->it_length += p_len_data;
        }
    }

    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           find a specified string in the specified buffer.
//  Param: p_lsbuf, specified buffer
//  Param: p_data_buf, a data buffer
//  Param: p_len_data, the number of bytes data stored in the buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_find(cyct_dstring_t* p_a_dstr, u08_t* p_data_buf, s32_t p_len_data)
{
    s32_t i_err_code = -1;

    s32_t i_free_size = CYCT_MIN_FREE_SIZE;
    u08_t* p_pos_find = NULL;

    if ((NULL == p_a_dstr) || (NULL == p_a_dstr->pt_string))
    {
        return i_err_code;
    }

    if (p_len_data > p_a_dstr->it_length)
    {
        return i_err_code;
    }

    p_pos_find = (u08_t*)strstr(p_a_dstr->pt_string, p_data_buf);
    if (NULL != p_pos_find)
    {
        i_err_code = (s32_t)(p_pos_find - p_a_dstr->pt_string);
    }

    return i_err_code;
}

/******************************************************************************/
//  DESCRIPTION:
//           Append and save data into the specified buffer.
//  Param: p_lsbuf, specified buffer
//  Param: p_data_buf, a data buffer
//  Param: p_len_data, the number of bytes data stored in the buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_merge(cyct_dstring_t* p_dst_buf, cyct_dstring_t* p_src_buf)
{
    s32_t i_op_status = 0;

    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if ((NULL == p_dst_buf) || (NULL == p_src_buf))
    {
        return i_op_status;
    }

    if ((NULL == p_src_buf->pt_string) || (0 == p_src_buf->it_size))
    {
        i_op_status = 1;
        return i_op_status;
    }

    if (NULL == p_dst_buf->pt_string)
    {
        p_dst_buf->pt_string = (u08_t *)malloc(p_src_buf->it_size + i_free_size);
        if (NULL == p_dst_buf->pt_string)
        {
            return i_op_status;
        }

        i_op_status = 1;
        p_dst_buf->it_size = p_src_buf->it_size + i_free_size;
        p_dst_buf->it_size = p_src_buf->it_size;
        memset(p_dst_buf->pt_string, 0, p_dst_buf->it_size);
        memcpy(p_dst_buf->pt_string, p_src_buf->pt_string, p_src_buf->it_size);
    }
    else
    {
        if ((p_src_buf->it_size + p_dst_buf->it_size) > (p_dst_buf->it_size - i_free_size))
        {
            s32_t u_total_len = 0;
            u08_t * p_tmp_ptr = NULL;

            u_total_len = p_src_buf->it_size + p_dst_buf->it_size;
            p_tmp_ptr = (u08_t *)malloc(u_total_len + i_free_size);
            if (NULL == p_tmp_ptr)
            {
                return i_op_status;
            }

            i_op_status = 1;
            memset(p_tmp_ptr, 0, u_total_len + i_free_size);
            memcpy(p_tmp_ptr, p_dst_buf->pt_string, p_dst_buf->it_size);
            memcpy(&(p_tmp_ptr[p_dst_buf->it_size]), p_src_buf->pt_string, p_src_buf->it_size);

            free(p_dst_buf->pt_string);
            p_dst_buf->pt_string = p_tmp_ptr;
            p_dst_buf->it_size = u_total_len + i_free_size;
            p_dst_buf->it_size = u_total_len;
        }
        else
        {
            i_op_status = 1;
            memcpy(&(p_dst_buf->pt_string[p_dst_buf->it_size]), p_src_buf->pt_string, p_src_buf->it_size);
            p_dst_buf->it_size += p_src_buf->it_size;
        }
    }

    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           Get ascii string from a specified buffer.
//  Param: p_lsbuf, longsung ftp buffer
//  Param: p_str_buffer, string buffer
//  Param: u_buf_size, the size of the string buffer;
//  Return: i_ret_len, the length of the string.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_get(cyct_dstring_t* p_a_dstr, u08_t* p_ascii_buf, s32_t i_buf_size)
{
    s32_t i_ret_len = 0;

    s32_t p_len_data = 0;
    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if ((NULL == p_a_dstr) || (NULL == p_a_dstr->pt_string))
    {
        return i_ret_len;
    }

    if (i_buf_size < p_a_dstr->it_length)
    {
        return i_ret_len;
    }

    memset(p_ascii_buf, 0, i_buf_size);
    memcpy(p_ascii_buf, p_a_dstr->pt_string, p_a_dstr->it_length);
    i_ret_len = p_a_dstr->it_length;

    return i_ret_len;
}

/******************************************************************************/
//  DESCRIPTION:
//           copy content of a ftp-buffer to another.
//  Param: [o]p_dst_buffer, 
//  Param: [i]p_src_buffer, 
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_copy(cyct_dstring_t* p_dst_buf, cyct_dstring_t* p_src_buf)
{
    s32_t i_op_status = 0;

    s32_t i_it_size = 0;
    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if (NULL == p_dst_buf)
    {
        return i_op_status;
    }

    if ((NULL == p_src_buf) || (NULL == p_src_buf->pt_string))
    {
        return i_op_status;
    }

    i_it_size = p_src_buf->it_size;
    if (NULL == p_dst_buf->pt_string)
    {
        p_dst_buf->pt_string = (u08_t *)malloc(i_it_size + i_free_size);
        if (NULL == p_dst_buf->pt_string)
        {
            return i_op_status;
        }

        p_dst_buf->it_size = i_it_size + i_free_size;
    }
    else
    {
        if (i_it_size > (p_dst_buf->it_size - i_free_size))
        {
            free(p_dst_buf->pt_string);
            p_dst_buf->pt_string = (u08_t *)malloc(i_it_size + i_free_size);
            if (NULL == p_dst_buf->pt_string)
            {
                return i_op_status;
            }

            p_dst_buf->it_size = i_it_size + i_free_size;
        }
    }

    i_op_status = 1;
    memset(p_dst_buf->pt_string, 0, i_it_size + i_free_size);
    memcpy(p_dst_buf->pt_string, p_src_buf->pt_string, i_it_size);
    p_dst_buf->it_size = i_it_size;

    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           clear the content of a ftp buffer and release related resources.
//  Param: [i]p_lsbuf, 
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_clear(cyct_dstring_t* p_a_dstring)
{
    s32_t i_op_status = -1;
    
    if (NULL == p_a_dstring)
    {
        return i_op_status;
    }
    
    i_op_status = 1;
    p_a_dstring->it_length = 0;
    if (NULL != p_a_dstring->pt_string)
    {
        memset(p_a_dstring->pt_string, 0, p_a_dstring->it_size);
    }
    
    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           compare the content of two buffers, 
//  Param: p_dst_buffer, specified buffer
//  Param: p_src_buffer, specified buffer
//  Return: i_is_equal, 0, equal; 1, not equal.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_compare(cyct_dstring_t* p_dst_buf, cyct_dstring_t* p_src_buf)
{
    s32_t i_is_equal = 1;

    if ((NULL == p_dst_buf) || (NULL == p_src_buf))
    {
        return i_is_equal;
    }

    if ((NULL == p_dst_buf->pt_string) || (NULL == p_src_buf->pt_string))
    {
        return i_is_equal;
    }

    if (p_dst_buf->it_size != p_src_buf->it_size)
    {
        return i_is_equal;
    }

    if (0 == memcmp(p_dst_buf->pt_string, p_src_buf->pt_string, p_dst_buf->it_size))
    {
        i_is_equal = 0;
    }

    return i_is_equal;
}

/******************************************************************************/
//  DESCRIPTION:
//           delete the specified buffer
//  Param: p_dst_buffer, specified buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_delete(cyct_dstring_t* p_a_dstring)
{
    s32_t i_op_status = 1;
    
    if (NULL == p_a_dstring)
    {
        return i_op_status;
    }
    
    if (NULL != p_a_dstring->pt_string)
    {
        free(p_a_dstring->pt_string);
        p_a_dstring->pt_string = NULL;
    }
    
    free(p_a_dstring);
    
    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           delete the specified buffer
//  Param: p_dst_buffer, specified buffer
//  Return: i_op_status, 0, fail; 1, success.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_upcase(cyct_dstring_t* p_a_dstr)
{
    s32_t i_op_status = 1;

    s32_t i = 0;
    
    if (NULL == p_a_dstr)
    {
        return i_op_status;
    }
    
    if (NULL != p_a_dstr->pt_string)
    {
        while (i < p_a_dstr->it_length)
        {
            if ((p_a_dstr->pt_string[i] >= 'a') && (p_a_dstr->pt_string[i] <= 'z'))
            {
                p_a_dstr->pt_string[i] = p_a_dstr->pt_string[i] - 'a' + 'A';
            }
            
            i++;
        }
        p_a_dstr->pt_string = NULL;
    }
    
    return i_op_status;
}

/******************************************************************************/
//  DESCRIPTION:
//           compare the content of two buffers, 
//  Param: p_dst_buffer, specified buffer
//  Param: p_src_buffer, specified buffer
//  Return: i_is_equal, 0, equal; 1, not equal.
//  History:
/******************************************************************************/
s32_t cyct_dstring_f_strncmp(cyct_dstring_t* p_dst_buf, u08_t* p_buf_data, s32_t p_len_data)
{
    s32_t i_is_equal = 1;
    
    if (NULL == p_dst_buf)
    {
        return i_is_equal;
    }
    
    if ((NULL == p_dst_buf->pt_string) || (NULL == p_buf_data))
    {
        return i_is_equal;
    }
    
    if (0 == memcmp(p_dst_buf->pt_string, p_buf_data, p_len_data))
    {
        i_is_equal = 0;
    }
    
    return i_is_equal;
}


s32_t cyct_dstring_f_deltail(cyct_dstring_t* p_dst_buf, u08_t c)
{
    s32_t i = 0;
    
    if ((NULL == p_dst_buf) || (NULL == p_dst_buf->pt_string))
    {
        return 0;
    }

    i = p_dst_buf->it_length - 1;
    while (i > 0)
    {
        if (p_dst_buf->pt_string[i] != c)
        {
            break;
        }

        i--;
    }

    p_dst_buf->it_length = i + 1;
    p_dst_buf->pt_string[p_dst_buf->it_length] = '\0';
    
    return 1;
}

s32_t cyct_dstring_f_getlength(cyct_dstring_t* p_a_dstring)
{
    s32_t i_len_dstr = 0;
    
    if ((NULL == p_a_dstring) || (NULL == p_a_dstring->pt_string))
    {
        return 0;
    }

    if (p_a_dstring->it_length < p_a_dstring->it_size)
    {
        i_len_dstr = p_a_dstring->it_length;
    }

    return i_len_dstr;
}

s32_t cyct_dstring_f_framelength(cyct_dstring_t* p_dst_buf, u08_t u_cur_endchar)
{
    s32_t i = 0;
    s32_t i_len_frame = 0;
    s32_t i_posi_framehead = 0;
    s32_t i_find_framehead = 0;
    s32_t i_posi_frametail = 0;
    s32_t i_find_frametail = 0;

    if ((NULL == p_dst_buf) || (NULL == p_dst_buf->pt_string))
    {
        return 0;
    }

    /* 当没有数据或者只有一个字符时，没有意义 */
    if (p_dst_buf->it_length <= 1)
    {
        return 0;
    }

    i = 1;
    while (i < p_dst_buf->it_length)
    {
        if ((p_dst_buf->pt_string[i - 1] == u_cur_endchar)
            && (p_dst_buf->pt_string[i] != u_cur_endchar))
        {
            i_posi_framehead = i - 1;
            i_find_framehead = 1;
            break;
        }
        i++;
    }

    if (i_find_framehead != 1)
    {
        p_dst_buf->it_length = 0;
        return 0;
    }

    if (i_posi_framehead != 0)
    {
        i = i_posi_framehead;
        memmove(&(p_dst_buf->pt_string[0]), &(p_dst_buf->pt_string[i]), p_dst_buf->it_length - i);
        p_dst_buf->it_length = p_dst_buf->it_length - i;
    }

    i = 1;
    while (i < p_dst_buf->it_length)
    {
        if ((p_dst_buf->pt_string[i - 1] != u_cur_endchar)
            && (p_dst_buf->pt_string[i] == u_cur_endchar))
        {
            i_posi_frametail = i;
            i_find_frametail = 1;
            i_len_frame = i + 1;
            break;
        }
        i++;
    }
    
    return i_len_frame;
}

s32_t cyct_dstring_f_read(cyct_dstring_t* p_cur_dstring, u08_t* p_buf_tryread, s32_t i_nb_tryread)
{
     s32_t i_ret_len = 0;

    s32_t p_len_data = 0;
    s32_t i_free_size = CYCT_MIN_FREE_SIZE;

    if ((NULL == p_cur_dstring) || (NULL == p_cur_dstring->pt_string))
    {
        return i_ret_len;
    }

    if (i_nb_tryread < p_cur_dstring->it_length)
    {
        memset(p_buf_tryread, 0, i_nb_tryread);
        memcpy(p_buf_tryread, p_cur_dstring->pt_string, i_nb_tryread);
        i_ret_len = i_nb_tryread;
        
        memmove(&(p_cur_dstring->pt_string[0]), &(p_cur_dstring->pt_string[i_nb_tryread]), p_cur_dstring->it_length - i_nb_tryread);
        p_cur_dstring->it_length = p_cur_dstring->it_length - i_nb_tryread;
    }

    return i_ret_len;
}

