#ifndef __CYCT_DEBUG_H__
#define __CYCT_DEBUG_H__

#ifdef CYCT_DEBUG
#  define OS_DBG_LOG(arg) do { cyct_runlog_f_myprintf arg; } while(0)
#  define OS_DBG_URC(arg) do { cyct_debug_f_printf arg; } while(0)
#  define OS_DBG_INF(arg) do { } while(0)
#  define OS_DBG_DBG(arg) do { } while(0)
#  define OS_DBG_ERR(arg) do { } while(0)
#  define OS_DBG_HEX(arg) do { } while(0)
#else /* CYCT_DEBUG */
#  define OS_DBG_LOG(arg) do { } while(0)
#  define OS_DBG_URC(arg) do { } while(0)
#  define OS_DBG_INF(arg) do { } while(0)
#  define OS_DBG_DBG(arg) do { } while(0)
#  define OS_DBG_ERR(arg) do { } while(0)
#  define OS_DBG_HEX(arg) do { } while(0)
#endif /* LXZAT_DEBUG */

int cyct_debug_f_printf(const char * vfmt, ...);

#endif /* __CYCT_DEBUG_H__ */
