#ifndef __CYCT_TYPES_H__
#define __CYCT_TYPES_H__

#ifndef u32_t
typedef unsigned int u32_t;
#endif

#ifndef u16_t
typedef unsigned short u16_t;
#endif

#ifndef u08_t
typedef unsigned char u08_t;
#endif

#ifndef s32_t
typedef int s32_t;
#endif

#ifndef s16_t
typedef short s16_t;
#endif

#ifndef s08_t
typedef char s08_t;
#endif

#endif /* __CYCT_TYPES_H__ */

