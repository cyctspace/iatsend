#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cyct_btime.h"

#define XSW_CUR_MAJOR 2
#define XSW_CUR_MINOR 2
#define XSW_CUR_REFIX 19

static int gi_init_flag = 0;
static int gi_cur_year = 2019;
static int gi_cur_month = 9;
static int gi_cur_day = 9;
static int gi_cur_hour = 9;
static int gi_cur_minute = 9;
static int gi_cur_second = 9;

static int gi_cur_timezone = 8;
static char gc_flag_timezone = '+';

static char gc_buf_btime[32];
static char gc_buf_version[32];

static int cyct_btime_f_init()
{
    int i_op_status = 0;
    if (0 == gi_init_flag)
    {
        int i_nb_get = 0;
        i_nb_get = sscanf(XSW_NOW, "%d %d %d %d %d %d %c%d",
            &gi_cur_year, &gi_cur_month, &gi_cur_day, &gi_cur_hour, 
            &gi_cur_minute, &gi_cur_second, &gc_flag_timezone, &gi_cur_timezone);
        if (8 == i_nb_get)
        {
            gi_init_flag = 1;
            i_op_status = 1;
        }
    }
    return i_op_status;
}

void* cyct_btime_f_get()
{
    char* p_xsw_btime = NULL;
    int i_op_status = 0;
    if (0 == gi_init_flag)
    {
        cyct_btime_f_init();
    }
    if (1 == gi_init_flag)
    {
        p_xsw_btime = gc_buf_btime;
        sprintf(p_xsw_btime, "%04d/%02d/%02d %02d:%02d:%02d%c%04d",
            gi_cur_year, gi_cur_month, gi_cur_day, gi_cur_hour, 
            gi_cur_minute, gi_cur_second, gc_flag_timezone, gi_cur_timezone);
    }
    return p_xsw_btime;
}

void* cyct_version_f_get()
{
    char* p_xsw_version = NULL;
    int i_op_status = 0;
    p_xsw_version = gc_buf_version;
    sprintf(p_xsw_version, "%d.%d.%d",
        XSW_CUR_MAJOR, XSW_CUR_MINOR, XSW_CUR_REFIX);
    return p_xsw_version;
}

/* --End-Of-File-- */

