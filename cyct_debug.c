#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"

int lxz_debug_f_printf(const char * vfmt, ...)
{
    int i = 0;
    int i_nb_data = 0;
	int i_nb_free = 0;

    char llbuf[8192];
    va_list ap;

	i_nb_free = sizeof(llbuf);
    memset(llbuf, 0x00, i_nb_free);
    va_start(ap,vfmt);
    i_nb_data = vsnprintf(llbuf, i_nb_free, vfmt, ap);
    va_end(ap);

    while (i<i_nb_data)
    {
        putchar(llbuf[i]);
        i++;
    }
    
    return i_nb_data;
}

