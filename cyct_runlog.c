#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <time.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"


static FILE* gfp_log_file = NULL;
static char gsp_buf_logfile[16384];

s32_t cyct_runlog_f_init(u08_t* u_rel_dir)
{
    int i_ret_val = 0;

    char llbuf[64];
    int i_str_len = 0;
    FILE* fp_pre_file = NULL;
    FILE* fp_log_file = NULL;
    s08_t* p_tmp_dir = NULL;
    s32_t i_dir_exist = 0;
    
    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;

    time(&atime);
    p = gmtime(&atime);

    memset(llbuf, 0, sizeof(llbuf));
    i_cur_year = p->tm_year+1900;
    i_cur_mon = p->tm_mon+1;
    i_cur_day = p->tm_mday;
    i_cur_hour = p->tm_hour+8;
    i_cur_min = p->tm_min;
    i_cur_sec = p->tm_sec;

#if 0
	sprintf(llbuf, "./lxzat%d%02d%02d%02d%02d%02d.log", i_cur_year, 
        i_cur_mon, i_cur_day, i_cur_hour, i_cur_min, i_cur_sec);
#else
    sprintf(llbuf, "./lxzat%d%02d%02d.log", i_cur_year, i_cur_mon, i_cur_day);
#endif

    fp_log_file = fopen(llbuf, "ab+");
    if (fp_log_file == NULL)
    {
        return i_ret_val;
    }

    i_ret_val = 1;
    fp_pre_file = gfp_log_file;
    gfp_log_file = fp_log_file;
    fflush(fp_log_file);

    if (fp_pre_file != NULL)
    {
        memset(llbuf, 0, sizeof(llbuf));
#if 0
        i_str_len = sprintf(llbuf, "%slxzat%d%02d%02d%02d%02d%02d.log", 
			"Next-logfile: ",i_cur_year, i_cur_mon, 
			i_cur_day, i_cur_hour, i_cur_min, i_cur_sec);
#else
        i_str_len = sprintf(llbuf, "%slxzat%d%02d%02d%02d%02d%02d.log", 
			"Next-logfile: ",i_cur_year, i_cur_mon, 
			i_cur_day, i_cur_hour, i_cur_min, i_cur_sec);
#endif
        fwrite(llbuf, 1, i_str_len, fp_pre_file);
        fflush(fp_pre_file);
        fclose(fp_pre_file);
    }

    return i_ret_val;
}

s32_t cyct_runlog_f_printf(const char * vfmt, ...)
{
    char llbuf[512];

    va_list ap;
    int i_nb_data = 0;
	int i_nb_free = 0;

    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        time(&atime);
        p = gmtime(&atime);
        
        i_cur_year = p->tm_year+1900;
        i_cur_mon = p->tm_mon+1;
        i_cur_day = p->tm_mday;
        i_cur_hour = p->tm_hour+8;
        i_cur_min = p->tm_min;
        i_cur_sec = p->tm_sec;

        i_nb_free = sizeof(llbuf);
        memset(llbuf, 0, i_nb_free);
        i_nb_data = sprintf(llbuf, "[%04d-%02d-%02d %02d:%02d:%02d]", 
            i_cur_year, i_cur_mon, i_cur_day, 
            i_cur_hour, i_cur_min, i_cur_sec);

        i_nb_free = i_nb_free - i_nb_data;
        va_start(ap,vfmt);
        i_nb_data = vsnprintf(&(llbuf[i_nb_data]), i_nb_free, vfmt, ap);
        va_end(ap);

        i_nb_data = strlen(llbuf);
        fwrite(llbuf, 1, i_nb_data, fp_log_file);
        fflush(fp_log_file);
    }
    
    return i_nb_data;
}

s32_t cyct_runlog_f_cprintf(const char * vfmt, ...)
{
    char ltbuf[128];
    char lsbuf[1023];

    va_list ap;
    int i = 0;
    int i_nb_time = 0;
    int i_nb_data = 0;
	int i_nb_free = 0;

    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        time(&atime);
        p = gmtime(&atime);
        
        i_cur_year = p->tm_year+1900;
        i_cur_mon = p->tm_mon+1;
        i_cur_day = p->tm_mday;
        i_cur_hour = p->tm_hour+8;
        i_cur_min = p->tm_min;
        i_cur_sec = p->tm_sec;

        memset(ltbuf, 0, sizeof(ltbuf));
        i_nb_time = sprintf(ltbuf, "[%04d-%02d-%02d %02d:%02d:%02d]", 
            i_cur_year, i_cur_mon, i_cur_day, 
            i_cur_hour, i_cur_min, i_cur_sec);

        i_nb_free = sizeof(lsbuf);
        va_start(ap,vfmt);
        i_nb_data = vsnprintf(&(lsbuf[0]), i_nb_free, vfmt, ap);
        va_end(ap);

        i = 0;
        while (i < i_nb_data)
        {
            if (lsbuf[i] == '\r')
            {
                fwrite("\r\n", 1, 2, fp_log_file);
                fwrite(ltbuf, 1, i_nb_time, fp_log_file);
            }
            else
            {
                if (lsbuf[i] != '\n')
                {
                    fwrite(&(lsbuf[i]), 1, 1, fp_log_file);
                }
            }
            i = i + 1;
        }
        fflush(fp_log_file);
    }
    
    return i_nb_data;
}

s32_t cyct_runlog_f_myprintf(const char * prefix, const char * vfmt, ...)
{
    char ltbuf[128];
    char * lsbuf = gsp_buf_logfile;
    int i_len_lsbuf = sizeof(gsp_buf_logfile);

    va_list ap;
    int i = 0;
    int i_nb_time = 0;
    int i_nb_data = 0;
	int i_nb_free = 0;
    int i_new_line = 0;

    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        time(&atime);
        p = gmtime(&atime);
        
        i_cur_year = p->tm_year+1900;
        i_cur_mon = p->tm_mon+1;
        i_cur_day = p->tm_mday;
        i_cur_hour = p->tm_hour;
        i_cur_min = p->tm_min;
        i_cur_sec = p->tm_sec;

        memset(ltbuf, 0, sizeof(ltbuf));
        i_nb_time = sprintf(ltbuf, "[%04d-%02d-%02d %02d:%02d:%02d]", 
            i_cur_year, i_cur_mon, i_cur_day, 
            i_cur_hour, i_cur_min, i_cur_sec);

        i_nb_free = i_len_lsbuf - 4;
        va_start(ap,vfmt);
        i_nb_data = vsnprintf(&(lsbuf[0]), i_nb_free, vfmt, ap);
        va_end(ap);

        fwrite(ltbuf, 1, i_nb_time, fp_log_file);
        if (NULL != prefix)
        {
            fwrite(prefix, 1, strlen(prefix), fp_log_file);
        }

        i = 0;
        while (i < i_nb_data)
        {
            if (lsbuf[i] == '\r')
            {
                if (i_new_line == 1)
                {
                    i_new_line = 0;
                    fwrite(ltbuf, 1, i_nb_time, fp_log_file);
                    if (NULL != prefix)
                    {
                        fwrite(prefix, 1, strlen(prefix), fp_log_file);
                    }
                }

                fwrite("\r\n", 1, 2, fp_log_file);
                i_new_line = 1;
            }
            else
            {
                if (lsbuf[i] != '\n')
                {
                    if (i_new_line == 1)
                    {
                        i_new_line = 0;
                        fwrite(ltbuf, 1, i_nb_time, fp_log_file);
                        if (NULL != prefix)
                        {
                            fwrite(prefix, 1, strlen(prefix), fp_log_file);
                        }
                    }
                    fwrite(&(lsbuf[i]), 1, 1, fp_log_file);
                }
            }
            i = i + 1;
        }
        fflush(fp_log_file);
    }
    
    return i_nb_data;
}

s32_t cyct_runlog_f_fsize(void)
{
    s32_t i_file_size = 0;
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        i_file_size = ftell(fp_log_file);
    }
    
    return i_file_size;
}

#define CYCT_DBG_OCTET_NUM 16
s32_t cyct_runlog_f_printfhex(const char * vbuf, s32_t ilen)
{
    s32_t i = 0;
    char * pbuf = (char *)vbuf;
    char lhexbuf[4 * CYCT_DBG_OCTET_NUM];
    
    cyct_runlog_f_printf("\r\n");
    cyct_runlog_f_printf("===HEX String Begin===\r\n");
    while (ilen >= CYCT_DBG_OCTET_NUM)
    {
        memset(lhexbuf, 0, sizeof(lhexbuf));
        
        i = 0;
        while (i < CYCT_DBG_OCTET_NUM)
        {
            sprintf(&(lhexbuf[3 * i]), "%02X ", pbuf[i]);
            i++;
        }

        ilen = ilen - CYCT_DBG_OCTET_NUM;
        pbuf = pbuf + CYCT_DBG_OCTET_NUM;
        
        cyct_runlog_f_printf("HEX: %s\r\n", lhexbuf);
    }
    
    if (ilen>0)
    {
        memset(lhexbuf, 0, sizeof(lhexbuf));

        i = 0;
        while (i < ilen)
        {
            sprintf(&(lhexbuf[3 * i]), "%02X ", pbuf[i]);
            i++;
        }

        pbuf = pbuf + ilen;
        ilen = 0;
        
        cyct_runlog_f_printf("HEX: %s\r\n", lhexbuf);
    }

    cyct_runlog_f_printf("===HEX String End===\r\n");
    cyct_runlog_f_printf("\r\n");
    
    return (pbuf - vbuf);
}

s32_t cyct_runlog_f_reset(void)
{
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        fclose(fp_log_file);
        gfp_log_file = fp_log_file = NULL;
    }

    return 1;
}


