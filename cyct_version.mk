#========================================
# A Simple Mafile for C/C++ Program
# Author: numax
# Date: 2016/09/21
#========================================


#========================================
# Get Current Operating System Name
#========================================
OS_NAME := $(shell uname -s)


#========================================
# All essential commands and tools
#========================================
RM := rm -rf
ECHO := echo
COPY := cp -f
MKDIR := mkdir
TOUCH := touch


#========================================
# All information related to version 
#========================================
XDBG_MAJOR := 2
XDBG_MINOR := 2
XDBG_REFIX := 16
XDBG_NOW := $(shell date "+%Y %m %d %H %M %S")

#========================================
# Current Project Name
#========================================
XDBG_CPN := building


#========================================
# All directorys for compiling 
#========================================
XDBG_RTDIR := xdbg
XDBG_MYDIR := $(XDBG_RTDIR)/$(XDBG_CPN)
XDBG_DIRS := $(XDBG_RTDIR) $(XDBG_MYDIR)


#========================================
# All files for compiling 
#========================================
XDBG_HFS := cyct_version.c

XDBG_HFS := $(addprefix $(XDBG_MYDIR)/, $(XDBG_HFS))


all:$(XDBG_DIRS) new
	@$(ECHO) OK

$(XDBG_DIRS):
	@$(MKDIR) $@

.PHONY:new check clean cleanall install
new:
	-@$(RM) $(XDBG_HFS)
	@$(ECHO) '#include <stdio.h>' > $(XDBG_HFS)
	@$(ECHO) '#include <stdlib.h>' >> $(XDBG_HFS)
	@$(ECHO) '#include <string.h>' >> $(XDBG_HFS)
	@$(ECHO) '#include <time.h>' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)


	@$(ECHO) -n '#define XSW_CUR_MAJOR ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MAJOR) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_MINOR ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MINOR) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_REFIX ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_REFIX) >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)

	@$(ECHO) -n '#define XSW_NOW ' >> $(XDBG_HFS)
	@$(ECHO) -n '"' >> $(XDBG_HFS)
	@$(ECHO) -n $(XDBG_NOW) >> $(XDBG_HFS)
	@$(ECHO) '"' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)


	@$(ECHO) 'static int gi_init_flag = 0;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_year = 2019;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_month = 9;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_day = 9;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_hour = 9;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_minute = 9;' >> $(XDBG_HFS)
	@$(ECHO) 'static int gi_cur_second = 9;' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)
	@$(ECHO) 'static char gc_buf_btime[32];' >> $(XDBG_HFS)
	@$(ECHO) 'static char gc_buf_version[32];' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)

	@$(ECHO) 'static int cyct_btime_f_init()' >> $(XDBG_HFS)
	@$(ECHO) '{' >> $(XDBG_HFS)
	@$(ECHO) '    int i_op_status = 0;' >> $(XDBG_HFS)
	@$(ECHO) '    if (0 == gi_init_flag)' >> $(XDBG_HFS)
	@$(ECHO) '    {' >> $(XDBG_HFS)
	@$(ECHO) '        int i_nb_get = 0;' >> $(XDBG_HFS)
	@$(ECHO) '        i_nb_get = sscanf(XSW_NOW, "%d %d %d %d %d %d",' >> $(XDBG_HFS)
	@$(ECHO) '            &gi_cur_year, &gi_cur_month, &gi_cur_day,' >> $(XDBG_HFS)
	@$(ECHO) '            &gi_cur_hour, &gi_cur_minute, &gi_cur_second);' >> $(XDBG_HFS)
	@$(ECHO) '        if (6 == i_nb_get)' >> $(XDBG_HFS)
	@$(ECHO) '        {' >> $(XDBG_HFS)
	@$(ECHO) '            gi_init_flag = 1;' >> $(XDBG_HFS)
	@$(ECHO) '            i_op_status = 1;' >> $(XDBG_HFS)
	@$(ECHO) '        }' >> $(XDBG_HFS)
	@$(ECHO) '    }' >> $(XDBG_HFS)
	@$(ECHO) '    return i_op_status;' >> $(XDBG_HFS)
	@$(ECHO) '}' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)


	@$(ECHO) 'void* cyct_btime_f_get()' >> $(XDBG_HFS)
	@$(ECHO) '{' >> $(XDBG_HFS)
	@$(ECHO) '    char* p_xsw_btime = NULL;' >> $(XDBG_HFS)
	@$(ECHO) '    int i_op_status = 0;' >> $(XDBG_HFS)
	@$(ECHO) '    if (0 == gi_init_flag)' >> $(XDBG_HFS)
	@$(ECHO) '    {' >> $(XDBG_HFS)
	@$(ECHO) '        cyct_btime_f_init();' >> $(XDBG_HFS)
	@$(ECHO) '    }' >> $(XDBG_HFS)
	@$(ECHO) '    if (1 == gi_init_flag)' >> $(XDBG_HFS)
	@$(ECHO) '    {' >> $(XDBG_HFS)
	@$(ECHO) '        p_xsw_btime = gc_buf_btime;' >> $(XDBG_HFS)
	@$(ECHO) '        sprintf(p_xsw_btime, "%04d/%02d/%02d %02d:%02d:%02d",' >> $(XDBG_HFS)
	@$(ECHO) '            gi_cur_year, gi_cur_month, gi_cur_day,' >> $(XDBG_HFS)
	@$(ECHO) '            gi_cur_hour, gi_cur_minute, gi_cur_second);' >> $(XDBG_HFS)
	@$(ECHO) '    }' >> $(XDBG_HFS)
	@$(ECHO) '    return p_xsw_btime;' >> $(XDBG_HFS)
	@$(ECHO) '}' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)


	@$(ECHO) 'void* cyct_version_f_get()' >> $(XDBG_HFS)
	@$(ECHO) '{' >> $(XDBG_HFS)
	@$(ECHO) '    char* p_xsw_version = NULL;' >> $(XDBG_HFS)
	@$(ECHO) '    int i_op_status = 0;' >> $(XDBG_HFS)
	@$(ECHO) '    p_xsw_version = gc_buf_version;' >> $(XDBG_HFS)
	@$(ECHO) '    sprintf(p_xsw_version, "%d.%d.%d",' >> $(XDBG_HFS)
	@$(ECHO) '        XSW_CUR_MAJOR, XSW_CUR_MINOR, XSW_CUR_REFIX);' >> $(XDBG_HFS)
	@$(ECHO) '    return p_xsw_version;' >> $(XDBG_HFS)
	@$(ECHO) '}' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)


	@$(ECHO) '/* --End-Of-File-- */' >> $(XDBG_HFS)
	@$(ECHO) '' >> $(XDBG_HFS)
	@$(COPY) $(XDBG_HFS) $(CURDIR)

check: 
	@$(ECHO) OS_NAME: $(OS_NAME)
	@$(ECHO) $(shell uname -a)

clean:
	-$(RM) $(XDBG_MYDIR)
	
cleanall:
	-$(RM) $(XDBG_RTDIR)

install:
	@$(ECHO) !!!Sorry, Unsupported Now!!!

