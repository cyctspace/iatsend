#ifndef __CYCT_ATCMD_H__
#define __CYCT_ATCMD_H__

s32_t cyct_prompt_f_xprintf(char* p_ascii_string, s32_t i_len_record);
s32_t cyct_prompt_f_dprintf(cyct_dstring_t* p_a_dstring, s32_t i_len_record);
cyct_dstring_t* cyct_atcmd_f_wait1(void* p_tty_ctxt, char* p_ascii_string1, s32_t i_cnt_milliseconds, s32_t* p_err_code);

#endif /* __CYCT_ATCMD_H__ */

