#ifndef __CYCT_DSTRING_H__
#define __CYCT_DSTRING_H__

typedef struct _cyct_dstring_t
{
    s32_t  it_size;
    s32_t  it_length;
    u08_t* pt_string;
}cyct_dstring_t;

cyct_dstring_t* cyct_dstring_f_new(u08_t* p_buf_data, s32_t u_len_data);
s32_t cyct_dstring_f_set(cyct_dstring_t* p_a_dstring, u08_t* p_buf_data, s32_t u_len_data);
s32_t cyct_dstring_f_append(cyct_dstring_t* p_a_dstring, u08_t* p_buf_data, s32_t u_len_data);
s32_t cyct_dstring_f_find(cyct_dstring_t* p_a_dstring, u08_t* p_buf_data, s32_t p_len_data);
s32_t cyct_dstring_f_merge(cyct_dstring_t* p_a_dstring, cyct_dstring_t* p_b_dstring);
s32_t cyct_dstring_f_get(cyct_dstring_t* p_a_dstring, u08_t* p_buf_data, s32_t u_buf_size);
s32_t cyct_dstring_f_copy(cyct_dstring_t* p_a_dstring, cyct_dstring_t* p_b_dstring);
s32_t cyct_dstring_f_clear(cyct_dstring_t* p_a_dstring);
s32_t cyct_dstring_f_compare(cyct_dstring_t* p_a_dstring, cyct_dstring_t* p_b_dstring);
s32_t cyct_dstring_f_delete(cyct_dstring_t* p_a_dstring);
s32_t cyct_dstring_f_upcase(cyct_dstring_t* p_a_dstring);
s32_t cyct_dstring_f_strncmp(cyct_dstring_t* p_a_dstring, u08_t* p_buf_data, s32_t p_len_data);
s32_t cyct_dstring_f_deltail(cyct_dstring_t* p_a_dstring, u08_t c);
s32_t cyct_dstring_f_getlength(cyct_dstring_t* p_a_dstring);
s32_t cyct_dstring_f_framelength(cyct_dstring_t* p_a_dstring, u08_t u_cur_endchar);
s32_t cyct_dstring_f_read(cyct_dstring_t* p_a_dstring, u08_t* p_buf_tryread, s32_t i_nb_tryread);

#endif /* __CYCT_DSTRING_H__ */


