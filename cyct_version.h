#ifndef __CYCT_VERSION_H__
#define __CYCT_VERSION_H__

void* cyct_btime_f_get();
void* cyct_version_f_get();

#endif /* __CYCT_VERSION_H__ */

