#include "cyct_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>

#include "cyct_types.h"
#include "cyct_runlog.h"
#include "cyct_debug.h"
#include "cyct_time.h"
#include "cyct_xstring.h"
#include "cyct_dstring.h"
#include "cyct_iotty.h"
#include "cyct_atcmd.h"


s32_t cyct_prompt_f_xprintf(char* p_ascii_string, s32_t i_len_record)
{
    s32_t i_len_string = 0;
    char* p_log_string = NULL;

    i_len_string = strlen(p_ascii_string);
    if (i_len_string <= 0)
    {
        return i_len_string;
    }

    p_log_string = p_ascii_string;
    if (i_len_record > 0)
    {
        p_log_string[i_len_record] = 0;
    }

    p_log_string = cyct_xstring_f_rstrip(p_log_string);
    p_log_string = cyct_xstring_f_lstrip(p_log_string);
    i_len_string = strlen(p_log_string);
    if (i_len_string > 0)
    {
        printf("%s\r\n", p_log_string);
    }

    return i_len_string;
}

s32_t cyct_prompt_f_dprintf(cyct_dstring_t* p_a_dstring, s32_t i_len_record)
{
    s32_t i_len_string = 0;
    char* p_log_string = NULL;

    if ((p_a_dstring == NULL) || (p_a_dstring->pt_string == NULL))
    {
        return i_len_string;
    }

    if (i_len_record > p_a_dstring->it_length)
    {
        return i_len_string;
    }

    p_log_string = p_a_dstring->pt_string;
    if (i_len_record > 0)
    {
        p_log_string[i_len_record] = 0;
    }

    p_log_string = cyct_xstring_f_rstrip(p_log_string);
    p_log_string = cyct_xstring_f_lstrip(p_log_string);
    i_len_string = strlen(p_log_string);
    if (i_len_string > 0)
    {
        printf("%s\r\n", p_log_string);
    }

    return i_len_string;
}

cyct_dstring_t* cyct_atcmd_f_wait1(void* p_tty_ctxt, char* p_ascii_string1, s32_t i_cnt_milliseconds, s32_t* p_err_code)
{
    s32_t i_err_code = 0;
 
    cyct_dstring_t* p_buf_allread = NULL;

    s32_t i_nb_curread = 0;
    u08_t p_buf_curread[128];

    u08_t* p_3gpp_error1 = "ERROR:";
    u08_t* p_3gpp_error2 = "\r\nERROR\r\n";

    u32_t i_cnt_cticks = 0;
    u32_t i_cnt_bticks = 0;
    u32_t i_cnt_eticks = 0;

	i_cnt_cticks = cyct_time_f_tickget();
	i_cnt_bticks = i_cnt_cticks;
	i_cnt_eticks = i_cnt_bticks + i_cnt_milliseconds;

    /* Get expected response or error */
    memset(p_buf_curread, 0, sizeof(p_buf_curread));
    i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_curread, sizeof(p_buf_curread));
    p_buf_allread = cyct_dstring_f_new(p_buf_curread, i_nb_curread);
    while (i_cnt_cticks < i_cnt_eticks)
    {
        if (cyct_dstring_f_find(p_buf_allread, p_ascii_string1, 0) >= 0)
        {
            i_err_code = 1;
            break;
        }
        if (cyct_dstring_f_find(p_buf_allread, p_3gpp_error1, 0) >= 0)
        {
            i_err_code = -1;
            break;
        }
        if (cyct_dstring_f_find(p_buf_allread, p_3gpp_error2, 0) >= 0)
        {
            i_err_code = -2;
            break;
        }

        memset(p_buf_curread, 0, sizeof(p_buf_curread));
        i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_curread, sizeof(p_buf_curread));
        if (i_nb_curread > 0)
        {
            cyct_dstring_f_append(p_buf_allread, p_buf_curread, i_nb_curread);
        }
 
        i_cnt_cticks = cyct_time_f_tickget();
        cyct_time_f_sleep(1);
    }

    /* Get the end of response or URC string */
    memset(p_buf_curread, 0, sizeof(p_buf_curread));
    i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_curread, sizeof(p_buf_curread));
    while (i_nb_curread > 0)
    {
        cyct_dstring_f_append(p_buf_allread, p_buf_curread, i_nb_curread);

        memset(p_buf_curread, 0, sizeof(p_buf_curread));
        i_nb_curread = cyct_iotty_read(p_tty_ctxt, p_buf_curread, sizeof(p_buf_curread));
    }

    (*p_err_code) = i_err_code;
    return p_buf_allread;
}

